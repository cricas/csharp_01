﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve
        public string Seed
        {
            get{ return this.seed; }
        }

        // TODO improve
        public string Name
        {
            get{ return name; }
        }

        // TODO improve
        public int Ordinal
        {
            get { return ordial; }
        }

        public override string ToString()
        {
            // TODO understand string interpolation
            return $"{this.GetType().Name}(Name={this.Name}, Seed={this.Seed}, Ordinal={this.Ordinal})";
        }

        public override bool Equals(object obj)
        {
            // TODO improve
            Card aux = (Card)obj;
            return (aux.name.Equals(Name))&&(aux.ordial.Equals(this.Ordinal));
        }

        public override int GetHashCode()
        {
            // TODO improve
            return base.GetHashCode();
        }
    }

}